#include <iostream>
#include <cmath>
#include <xmmintrin.h>
#include <time.h>
using namespace std;
#define N 2048
#define M 10
float maxJ(float* A){
    int j = 0; float max = 0, sum = 0;
    for (j = 0; j < N; j++){
        for (int i = 0; i < N; i++){
            sum += abs(A[i*N + j]);
        }
        if (sum > max) max = sum;
        sum = 0;
    }
    return max;
}

float maxI(float* A){
    int j = 0; float max = 0, sum = 0;
    for (int i = 0; i < N; i++){
        for (j = 0; j < N; j++){
            sum += abs(A[i*N + j]);
        }
        if (sum > max) max = sum;
        sum = 0;
    }
    return max;
}

void matSum(float *A, float *B, float *R){ //sum 2 matrix
    __m128 *AA, *BB, *RR;
    AA = (__m128*)A;
    BB = (__m128*)B;
    RR = (__m128*)R;
    for (int i = 0; i < N*N/4; i++){
        RR[i] = _mm_add_ps(AA[i], BB[i]);
    }
}

void matMult(float *A, float *B, float *R){
    __m128 *AA, *BB;
    __m128 mul = _mm_setzero_ps(), res = _mm_setzero_ps();
    int i, j, z;
    AA = (__m128*)A;
    BB = (__m128*)B;
    for (i = 0; i < N; i++){
        for (j = 0; j < N; j++){
            res = _mm_setzero_ps();
            for (z = 0; z < N/4; z++){
                mul = _mm_mul_ps(AA[i*N/4 + z], BB[j*N/4 + z]);
                res = _mm_add_ps(res, mul);
            }
            mul = _mm_movehl_ps(mul, res);
            res = _mm_add_ps(res, mul);
            mul = _mm_shuffle_ps(res, res, 1);
            res = _mm_add_ss(res, mul);
            _mm_store_ss( (R + i*N + j), res);
        }

    }
}

int main() {
    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    float maxi = 0.0, maxj = 0.0;
    int i = 0, j = 0, k = 0;

    float *A = (float*)_mm_malloc(N*N*sizeof(float), 16);
    float *BA = (float*)_mm_malloc(N*N*sizeof(float), 16);
    float *B = (float*)_mm_malloc(N*N*sizeof(float), 16);
    float *R = (float*)_mm_malloc(N*N*sizeof(float), 16);
    float *I = (float*)_mm_malloc(N*N*sizeof(float), 16);
    float *IR = (float*)_mm_malloc(N*N*sizeof(float), 16);
    float *resA = (float*)_mm_malloc(N*N*sizeof(float), 16);
    float *degR = (float*)_mm_malloc(N*N*sizeof(float), 16);
    float *Z = (float*)_mm_malloc(N*N*sizeof(float), 16);
    float *Atr = (float*)_mm_malloc(N*N*sizeof(float), 16);



    j = 1;
    for (i = 0; i < N*N; i++){      //Initializing A, BA, I
        A[i] = (float) (rand() % 10);//i % N + k;
        if (i != 0 && i % N == 0) k++;
        if (i%N == 0) A[i] = k;
        BA[i] = 0;
        R[i] = 0;
        if (i % j == 0){
            I[i] = 1;
            if (j != 1) j += N + 1; 
            else j = N + 1;
        }
        else I[i] = 0;
    }
    A[0] = 1;
    A[1] = 2;
    A[N] = 2;

    //matrix B:
    j = 0, k =0;
    for (j = 0; j < N; j++){
        for (i = 0; i < N; i++) {
            B[j * N + i] = A[i * N + j];
            B[i * N + j] = A[j * N + i];
        }
    }

/*   cout << "Matrix I:\n";
    for (i = 0; i < N*N; i++){
        if (i%N == 0) cout << endl;
        cout << I[i] << ' ';
    }
*/
/*    cout << "\nMatrix A:\n";
    for (i = 0; i < N*N; i++){
        if (i%N == 0) cout << endl;
        cout << A[i] << ' ';
    } */

    for (i = 0; i < N; i++){
        for (j = 0; j < N; j++){
            B[N*i + j] = (A[N*j + i])/(maxI(A) * maxJ(A));
            Atr[N*i + j] = A[N*j + i];
        }
    }

    matMult(B, Atr, Z); //Z = AB, B = A^T^T
	cout << "I'm here!!!";
    for (i = 0; i < N*N; i++){
        Z[i] = -Z[i];
    }

    matSum(I, Z, R); //R = I - BA
    for (i = 0; i < N*N; i++){ //Save R and I to next operations
        A[i] = R[i];
        IR[i] = I[i];
    }

    matSum(IR, R, IR);
    for (i = 0; i < M; i++){
        matMult(A, R, degR); //degR = R * R^(i-1)
        for (j = 0; j < N*N; j++) A[j] = degR[j]; //R = R^j
        matSum(IR, degR, IR); // IR += R^i
    }

    for (i = 0; i < N; i++){
        for (j = 0; j < N; j++){
            Z[N*i + j] = B[N*j + i];
        }
    };
    matMult(IR, Z, resA); //resA = IR * B




/*    cout << "\nMatrix A^(-1):";
    for (i = 0; i < N*N; i++){
        if (i%N == 0) cout << endl;
        cout << resA[i] << ' ';
    }
    cout <<endl;*/
	clock_gettime(CLOCK_MONOTONIC_RAW, &end);
	printf("Time taken: %lf sec.\n", end.tv_sec - start.tv_sec + 0.000000001*(end.tv_nsec - start.tv_nsec));   
 _mm_free(A);
    _mm_free(B);
    _mm_free(BA);
    _mm_free(I);
    _mm_free(R);
    //_mm_free(resA);
    //_mm_free(IR);
    return 0;
}

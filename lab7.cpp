#include <time.h>
#include <iostream>
#include <cmath>
using namespace std;

void matrix(int N, float **A, float **B, float** res){
    float sum = 0;
    for (int k = 0; k < N; k++){
        for (int i = 0; i < N; i++){
            for (int j = 0; j < N; j++){
                sum += A[k][j] * B[j][i];
            }
            res[k][i] = sum;
            sum = 0;
        }
    }
}

int main(int argc, char* argv[]) {
    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    int N = atoi(argv[1]);
    int M = atoi(argv[2]);
    int i, j, k;
    auto **A = new float*[N];
    auto **BA = new float*[N];
    auto B = new float*[N];
    auto **R = new float*[N];
    auto **I = new float*[N];
    auto **resA = new float*[N];
    auto **degR = new float*[N];
    for (i = 0; i < N; i++) {
        A[i] = new float[N];
        B[i] = new float[N];
        BA[i] = new float[N];
        R[i] = new float[N];
        I[i] = new float[N];
        resA[i] = new float[N];
        degR[i] = new float[N];
    }

    float maxi = 0;
    float maxj = 0;
    float sum = 0;



    for (i = 0; i < N; i++){
        for (j = 0; j < N; j++){
            A[i][j] = i + j;
            BA[j][i] = 0;
            if (i == j) I[i][j] = 1;
            else I[i][j] = 0;
        }
    }
    A[0][0] = 1;

    for (j = 0; j < N; j++){
        for (i = 0; i < N; i++) sum += abs(A[i][j]);
        if (sum > maxi) maxi = sum;

        sum = 0;
    }

//    cout << "Matrix A:\n";

    for (i = 0; i < N; i++){
        for (j = 0; j < N; j++) {
            sum += abs(A[i][j]);
  //          cout << A[i][j] << ' ';         //TEST
        }
//        cout << endl;                       //TEST
        if (sum > maxj) maxj = sum;
        sum = 0;
    }

        for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            B[i][j] = A[j][i] / (maxj * maxi);
            //cout << B[i][j] << ' ';
        }
        //cout << endl;
    }

    matrix(N, B, A, BA);

    /*cout << "\nMatrix BA:\n";
    for (i = 0; i < N; i++){
        for (j = 0; j < N; j++){
            cout << BA[i][j] << ' ';
        }
        cout << endl;
    }*/

    for (i = 0; i < N; i++){
        for (j = 0; j < N; j++){
            R[i][j] = I[i][j] - BA[i][j];
            A[i][j] = R[i][j];
            degR[i][j] = R[i][j];
        }
    }

    /*cout << "\nMatrix R:\n";
    for (i = 0; i < N; i++){
        for (j = 0; j < N; j++){
            cout << R[i][j] << ' ';
        }
        cout <<endl;
    }*/

    for (k = 0; k < M; k++){
        for (i = 0; i < N; i++){
            for (j = 0; j < N; j++){
                I[i][j] += degR[i][j];
                R[i][j] = degR[i][j];
            }
        }
        matrix(N, R, A, degR);
        /*cout << "\nMatrix R" << k <<endl;
        for (int h = 0; h < N; h++){
            for (int g = 0; g < N; g++){
                cout << I[h][g] << ' ';
            }
            cout << endl;
        }*/
    }

    /*cout << "\nMatrix I:\n";
    for (i = 0; i < N; i++){
        for (j = 0; j < N; j++){
            cout << I[i][j];
        }
        cout << endl;
    }*/

    matrix(N, I, B, resA);
    /*cout << "\nA^(-1) matrix:\n";
    for (i = 0; i < N; i++){
        for (j = 0; j < N; j++){
            cout << resA[i][j] << ' ';
        }
        cout << endl;
    }*/

    for (i = 0; i < N; i++) {
        delete[] A[i];
        delete[] B[i];
        delete[] BA[i];
        delete[] I[i];
        delete[] R[i];
        delete[] resA[i];
    }

    delete[] A;
    delete[] B;
    delete[] BA;
    delete[] I;
    delete[] R;
    delete[] resA;
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    printf("Time taken: %lf sec.\n",end.tv_sec-start.tv_sec + 0.000000001*(end.tv_nsec-start.tv_nsec));
    return 0;
}
